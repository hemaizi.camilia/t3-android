package com.example.uapv1900102.tp3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author HEMAIZI Camilia
 */
public class NewCityActivity extends AppCompatActivity {
    WeatherDbHelper dbHelper;

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        dbHelper= new WeatherDbHelper(this);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);






        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sauvegarder();

            }
        });
    }

    public void sauvegarder(){
        City newcity = new City();
        newcity.setName(textName.getText().toString());
        newcity.setCountry(textCountry.getText().toString());

        boolean ajout=dbHelper.addCity(newcity);

        /*
         * afficher un message pour informer que la requete a etait executer avec succes
         */
        if(ajout==true){
            android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Ville ajouté avec succes !")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent2 = new Intent(NewCityActivity.this, MainActivity.class);
                            startActivity(intent2);
                        }
                    })
                    .show();
        }
    }
}
