package com.example.uapv1900102.tp3;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * @author HEMAIZI Camilia
 */
public class MainActivity extends AppCompatActivity  {
    WeatherDbHelper dbHelper;
    Intent intent;
    ListView listView;
    SimpleCursorAdapter cursorAdapter;
    SwipeRefreshLayout swipeLayout;
    JSONResponseHandler json;
    InputStream is;
    City city;
    Cursor cursor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = findViewById(R.id.liste);
        dbHelper = new WeatherDbHelper(this);
        FloatingActionButton fab = findViewById(R.id.fab);

        /*
         * fonction qui permet d'ajouter a la base de données  et recuperer les champs et les afficher dans la listview
         */
        this.affiche();

        swipeLayout = findViewById(R.id.swipeRefreshLayout);


        /*
        * Fonction qui s'execute lors que on actualise la liste
         */
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            ExecuteTask task;
            @Override
            public void onRefresh() {

                cursor = dbHelper.fetchAllCities();
                city = dbHelper.cursorToCity(cursor);

                while(cursor.moveToNext()){
               try {

                   Log.d("*****************************************************",city.getName());

                   URL url = WebServiceUrl.build(city.getName(),city.getCountry());
                   task = new ExecuteTask();
                   task.execute(url.toString());
                   city = dbHelper.cursorToCity(cursor);
               }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                }

               }
            }
        });


        /**
         * Button qui permet l'ajout d'une nouvelle ville
         */
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivity(intent);
            }
        });

        /*
         * Afficher les informations dune ville en cliquant sur l'item de la liste view
         */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {

                intent = new Intent(MainActivity.this, CityActivity.class);

                Cursor cursor = dbHelper.fetchAllCities();
                cursor.moveToPosition(position);
                City city = dbHelper.cursorToCity(cursor);

                intent.putExtra("city", city);
                startActivityForResult(intent, 0);
            }

        });
    }


    @Override
    public void onResume() {
        super.onResume();
        cursorAdapter.changeCursor(dbHelper.fetchAllCities());
        cursorAdapter.notifyDataSetChanged();
    }


    // probleme de icon a cause de bitmap
    public void affiche() {

        //dbHelper.populate();
        Cursor cursor = dbHelper.fetchAllCities();

        cursorAdapter = new SimpleCursorAdapter(this, R.layout.row,
               cursor, new String[]{WeatherDbHelper.COLUMN_ICON ,WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY, WeatherDbHelper.COLUMN_TEMPERATURE}
                , new int[]{R.id.imageViewRow,R.id.cName, R.id.cCountry, R.id.temperature}, 0);
        listView.setAdapter(cursorAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public class ExecuteTask extends AsyncTask<String, Void, String> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeLayout.setRefreshing(true);

        }

        @Override
        protected String doInBackground(String... strings) {

            String result= "";
            URL url;
            HttpURLConnection urlConnection=null;

            try
            {

                url = new URL(strings[0]);
                urlConnection= (HttpURLConnection) url.openConnection();
                is = urlConnection.getInputStream();

                json = new JSONResponseHandler(city);
                Log.d("*****************************************************",city.getName());

              json.readJsonStream(is);

                return result;
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * Modifier les données des villes dans la base de données et rafrishir la list
         */
        @Override
        protected void onPostExecute(String s){
            super.onPostExecute(s);

            try {
                 dbHelper.updateCity(city);
             cursorAdapter.changeCursor(dbHelper.fetchAllCities());
             cursorAdapter.notifyDataSetChanged();
                swipeLayout.setRefreshing(false);
               // city = dbHelper.cursorToCity(cursor);


            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

    }



}